@echo off

setlocal EnableDelayedExpansion

set command=
set argCount=0
for %%x in (%*) do (
   set command=!command! %%~x
)

::echo %command%

SET command=%command:\"=%

::echo %command%>> %~dp0\arg_adaptor.log

%command%

endlocal
